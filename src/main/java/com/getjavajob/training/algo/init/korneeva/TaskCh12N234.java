package com.getjavajob.training.algo.init.korneeva;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by ZinZaga on 14.08.16.
 */
public class TaskCh12N234 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the row:");
        int row = scanner.nextInt();
        System.out.println("Enter the column:");
        int column = scanner.nextInt();
        Arrays.toString(deleteRow(row));
        System.out.println();
        Arrays.toString(deleteColumn(column));
    }

    public static int[][] deleteRow(int row) {
        int k = 1;
        int[][] a = new int[5][5];
        int[][] b = new int[5][5];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                a[i][j] = k++;
            }
        }
        for (int i = 0; i < 4; i++) {
            if (i + 1 == row) {
                for (int j = 0; j < 5; j++) {
                    a[i][j] = a[i + 1][j];
                }
                row++;
            }
        }
        System.arraycopy(a, 0, b, 0, 4);
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.print(b[i][j] + " ");
            }
            System.out.println();
        }
        return b;
    }

    public static int[][] deleteColumn(int column) {
        int k = 1;
        int[][] a = new int[5][5];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                a[i][j] = k++;
            }
        }
        for (int j = 0; j < 4; j++) {
            if (j + 1 == column) {
                for (int i = 0; i < 5; i++) {
                    a[i][j] = a[i][j + 1];
                }
                column++;
            }
        }
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (i >= 0 && j == 4) {
                    a[i][j] = 0;
                }
            }
        }
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
        return a;
    }
}
