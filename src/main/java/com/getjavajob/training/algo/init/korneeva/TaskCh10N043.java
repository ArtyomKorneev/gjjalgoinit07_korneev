package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 03.08.16.
 */
public class TaskCh10N043 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number:");
        int number = scanner.nextInt();
        System.out.println(amountCalculation(number));
        System.out.println(calculationOfTheNumberOfDigits(number));
    }

    public static int amountCalculation(int number) {
        if (number < 10) {
            return number;
        } else {
            return number % 10 + amountCalculation(number / 10);
        }
    }

    public static int calculationOfTheNumberOfDigits(int number) {
        if (number < 10) {
            return 1;
        } else {
            return calculationOfTheNumberOfDigits(number / 10) + 1;
        }
    }
}