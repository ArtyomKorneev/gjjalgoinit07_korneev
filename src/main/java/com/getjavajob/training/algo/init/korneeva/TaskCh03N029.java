package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 19.07.16.
 */
public class TaskCh03N029 {
    public static void main(String[] args) {
        System.out.println("Enter three numbers");
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int y = scanner.nextInt();
        int z = scanner.nextInt();
        System.out.println(checkOdd(x, y));
        System.out.println(lessThanTwenty(x, y));
        System.out.println(checkZero(x, y));
        System.out.println(checkNegativity(x, y, z));
        System.out.println(divideIntoFive(x, y, z));
        System.out.println(moreThanAHundred(x, y, z));
    }

    /**
     * This method returns true when each number is odd.
     */
    public static boolean checkOdd(int x, int y) {
        return x % 2 == 1 && y % 2 == 1;
    }

    /**
     * This method returns true when only one number less than 20.
     */
    public static boolean lessThanTwenty(int x, int y) {
        return x < 20 ^ y < 20;
    }

    /**
     * This method returns true when at least one of the numbers equal to zero.
     */
    public static boolean checkZero(int x, int y) {
        return x == 0 || y == 0;
    }

    /**
     * This method returns true when each number is negative.
     */
    public static boolean checkNegativity(int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    /**
     * This method returns true when only one number multiplicity by 5.
     */
    public static boolean divideIntoFive(int x, int y, int z) {
        boolean numberX = x % 5 == 0;
        boolean numberY = y % 5 == 0;
        boolean numberZ = z % 5 == 0;
        return (numberX ^ numberY) ? (!numberZ) : numberZ && (!numberX);
    }

    /**
     * This method returns true when at least one of the numbers bigger than 100.
     */
    public static boolean moreThanAHundred(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}
