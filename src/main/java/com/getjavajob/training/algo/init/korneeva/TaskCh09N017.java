package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 27.07.16.
 */
public class TaskCh09N017 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the word:");
        String word = scanner.nextLine();
        System.out.println(compareFirstAndLastLetter(word));
    }

    public static boolean compareFirstAndLastLetter(String word) {
        return word.charAt(0) == word.charAt(word.length() - 1);
    }
}
