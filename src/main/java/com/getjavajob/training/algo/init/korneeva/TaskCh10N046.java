package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 04.08.16.
 */
public class TaskCh10N046 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the first number:");
        int first = scanner.nextInt();
        System.out.println("Enter the denominator:");
        int denominator = scanner.nextInt();
        System.out.println("Enter the n:");
        int n = scanner.nextInt();
        System.out.println("n is " + geometryProgression(first, denominator, n));
        System.out.println("Sum is " + sumOfThree(first, denominator, n));
    }

    public static int geometryProgression(int first, int denominator, int n) {
        if (n == 1) {
            return first;
        } else {
            return geometryProgression(first * denominator, denominator, --n);
        }
    }

    public static int sumOfThree(int first, int denominator, int n) {
        if (n == 1) {
            return first;
        } else {
            return sumOfThree(first * denominator, denominator, --n) + first;
        }
    }
}