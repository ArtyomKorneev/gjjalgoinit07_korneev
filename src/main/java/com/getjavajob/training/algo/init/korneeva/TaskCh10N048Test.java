package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh10N048.findMax;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 11.08.16.
 */
public class TaskCh10N048Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh10N048Test.test", 324, findMax(new int[]{1, 4, 34, 6, 324}, 0));
    }
}
