package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh09N166.changeWords;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 10.08.16.
 */
public class TaskCh09N166Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh09N166Test.test", "[way, the, by]", changeWords("by the way"));
    }
}
