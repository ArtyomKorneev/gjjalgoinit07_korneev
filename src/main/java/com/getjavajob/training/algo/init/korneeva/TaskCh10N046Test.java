package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh10N046.geometryProgression;
import static com.getjavajob.training.algo.init.korneeva.TaskCh10N046.sumOfThree;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 11.08.16.
 */
public class TaskCh10N046Test {
    public static void main(String[] args) {
        test1();
        test2();
    }

    private static void test1() {
        assertEquals("TaskCh10N046Test.test", 16, geometryProgression(1, 2, 5));
    }

    private static void test2() {
        assertEquals("TaskCh10N046Test.test", 31, sumOfThree(1, 2, 5));
    }
}
