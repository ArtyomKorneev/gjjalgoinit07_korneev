package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 21.07.16.
 */
public class TaskCh05N064 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] citizens = new int[12];
        int[] areas = new int[12];
        System.out.println("Enter the number of citizens:");
        for (int i = 0; i < citizens.length; i++) {
            citizens[i] = scanner.nextInt();
        }
        System.out.println("Enter square of areas:");
        for (int i = 0; i < areas.length; i++) {
            areas[i] = scanner.nextInt();
        }
        int result = getPopulationDensity(citizens, areas);
        System.out.println("The average population density " + result);
    }

    public static int getPopulationDensity(int[] citizens, int[] areas) {
        int sumCitizens = 0;
        for (int i = 0; i < citizens.length; i++) {
            sumCitizens += citizens[i];
        }
        int sumAreas = 0;
        for (int i = 0; i < areas.length; i++) {
            sumAreas += areas[i];
        }
        return sumCitizens / sumAreas;
    }
}
