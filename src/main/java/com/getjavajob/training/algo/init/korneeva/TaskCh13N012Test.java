package com.getjavajob.training.algo.init.korneeva;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 24.08.16.
 */
public class TaskCh13N012Test {
    public static void main(String[] args) {
        testGetNumOfYearsWorked();
        testSearchEmployeesBySubstring();
        testSearchEmployeesByYear();
    }

    private static void testGetNumOfYearsWorked() {
        Employee testEmployee = new Employee("Pushkin", "Alexander", "Sergeevich", "Arbatskaya 7", 10, 2000);
        assertEquals("TaskCh13N012Test.testGetNumOfYearsWorked", 15, testEmployee.getNumOfYearsWorked(7, 2016));
    }

    private static void testSearchEmployeesBySubstring() {
        Database data = new Database();
        data.addEmployees(new Employee("Ivanov", "Ivan", "Ivanovich", "Pushkina 8", 1, 1995));
        data.addEmployees(new Employee("Petrov", "Semen", "Kolotushkina 9", 2, 1996));
        data.addEmployees(new Employee("Alba", "Jessica", "Petrovna", "District 7", 3, 1997));
        data.addEmployees(new Employee("Korneev", "Artyom", "Aleksandrovich", "Ostozhenka 17", 4, 1998));
        data.addEmployees(new Employee("Sidorov", "Vladimir", "Sergeevich", "Achunova 6", 5, 1999));
        data.addEmployees(new Employee("Drozdov", "Andrew", "Ivanovich", "District 5", 6, 2000));
        data.addEmployees(new Employee("Volya", "Pavel", "Nikolaevich", "5 avenue", 7, 2001));
        data.addEmployees(new Employee("Egorov", "Andrew", "Yurevich", "Sacharova, 32", 8, 2002));
        data.addEmployees(new Employee("Gilev", "Kirill", "Semenovich", "Pushkina 8", 9, 2003));
        data.addEmployees(new Employee("Pushkin", "Alexander", "Sergeevich", "Arbatskaya 7", 10, 2000));
        data.addEmployees(new Employee("Lermontov", "Mihail", "Yurevich", "Tverskaya 5", 11, 2001));
        data.addEmployees(new Employee("Martin", "Lutter", "King", "Achunova 6", 12, 2002));
        data.addEmployees(new Employee("Suvorov", "Alexander", "Vasilevich", "District 12", 1, 2003));
        data.addEmployees(new Employee("Davidov", "Denis", "5 avenue", 2, 2004));
        data.addEmployees(new Employee("Lenin", "Vladimir", "Ilich", "Sacharova, 32", 3, 2005));
        data.addEmployees(new Employee("Stalin", "Iosif", "Vissarionovich", "Kirova 76", 4, 2006));
        data.addEmployees(new Employee("Marks", "Carl", "Camala 93", 5, 2007));
        data.addEmployees(new Employee("Kovalevskaya", "Sofia", "Koroleva 100", 6, 2008));
        data.addEmployees(new Employee("Kulikova", "Lubov", "Ivanovna", "Glushko 47", 7, 2009));
        data.addEmployees(new Employee("Dragunova", "Mariya", "Andreevna", "Zinina 4", 8, 2010));
        List<Employee> testEmployees1 = new ArrayList<>();
        testEmployees1.add(new Employee("Lenin", "Vladimir", "Ilich", "Sacharova, 32", 3, 2005));
        assertEquals("TaskCh13N012Test.testSearchEmployeesBySubstring", testEmployees1, data.searchEmployeesBySubstring("lenin"));
    }

    private static void testSearchEmployeesByYear() {
        Database data = new Database();
        data.addEmployees(new Employee("Ivanov", "Ivan", "Ivanovich", "Pushkina 8", 1, 1995));
        data.addEmployees(new Employee("Petrov", "Semen", "Kolotushkina 9", 2, 1996));
        data.addEmployees(new Employee("Alba", "Jessica", "Petrovna", "District 7", 3, 1997));
        data.addEmployees(new Employee("Korneev", "Artyom", "Aleksandrovich", "Ostozhenka 17", 4, 1998));
        data.addEmployees(new Employee("Sidorov", "Vladimir", "Sergeevich", "Achunova 6", 5, 1999));
        data.addEmployees(new Employee("Drozdov", "Andrew", "Ivanovich", "District 5", 6, 2000));
        data.addEmployees(new Employee("Volya", "Pavel", "Nikolaevich", "5 avenue", 7, 2001));
        data.addEmployees(new Employee("Egorov", "Andrew", "Yurevich", "Sacharova, 32", 8, 2002));
        data.addEmployees(new Employee("Gilev", "Kirill", "Semenovich", "Pushkina 8", 9, 2003));
        data.addEmployees(new Employee("Pushkin", "Alexander", "Sergeevich", "Arbatskaya 7", 10, 2000));
        data.addEmployees(new Employee("Lermontov", "Mihail", "Yurevich", "Tverskaya 5", 11, 2001));
        data.addEmployees(new Employee("Martin", "Lutter", "King", "Achunova 6", 12, 2002));
        data.addEmployees(new Employee("Suvorov", "Alexander", "Vasilevich", "District 12", 1, 2003));
        data.addEmployees(new Employee("Davidov", "Denis", "5 avenue", 2, 2004));
        data.addEmployees(new Employee("Lenin", "Vladimir", "Ilich", "Sacharova, 32", 3, 2005));
        data.addEmployees(new Employee("Stalin", "Iosif", "Vissarionovich", "Kirova 76", 4, 2006));
        data.addEmployees(new Employee("Marks", "Carl", "Camala 93", 5, 2007));
        data.addEmployees(new Employee("Kovalevskaya", "Sofia", "Koroleva 100", 6, 2008));
        data.addEmployees(new Employee("Kulikova", "Lubov", "Ivanovna", "Glushko 47", 7, 2009));
        data.addEmployees(new Employee("Dragunova", "Mariya", "Andreevna", "Zinina 4", 8, 2010));
        List<Employee> testEmployees2 = new ArrayList<>();
        testEmployees2.add(new Employee("Ivanov", "Ivan", "Ivanovich", "Pushkina 8", 1, 1995));
        assertEquals("TaskCh13N012Test.testSearchEmployeesByYear", testEmployees2, data.searchEmployeesByYear(7, 2016, 20));
    }

}
