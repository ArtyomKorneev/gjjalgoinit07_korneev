package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh02N031.moveFigures;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 09.08.16.
 */
public class TaskCh02N031Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        int result = moveFigures(123);
        assertEquals("TaskCh02N013Test", 132, result);
    }
}
