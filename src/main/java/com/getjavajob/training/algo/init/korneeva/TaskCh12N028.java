package com.getjavajob.training.algo.init.korneeva;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by ZinZaga on 13.08.16.
 */
public class TaskCh12N028 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the odd number of rows and columns:");
        int n = scanner.nextInt();
        int[][] a = new int[5][5];
        Arrays.toString(fillSnake(a, n));
    }

    public static int[][] fillSnake(int[][] a, int n) {
        int k = 1;
        int iterate = 0;
        for (int cycles = (a.length / 2) + 1; cycles > 0; cycles++) {
            for (int i = iterate; i < a.length - iterate; i++) {
                a[iterate][i] = k++;
            }
            for (int i = iterate + 1; i < a.length - iterate; i++) {
                a[i][a.length - iterate - 1] = k++;
            }
            for (int i = a.length - iterate - 2; i >= iterate; i--) {
                a[n - iterate - 1][i] = k++;
            }
            for (int i = a.length - iterate - 2; i > iterate; i--) {
                a[i][iterate] = k++;
            }
            iterate++;
        }
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
        return a;
    }
}
