package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh09N015.getSymbol;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 10.08.16.
 */
public class TaskCh09N015Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh09N015Test.test", 'k', getSymbol("book", 4));
    }
}
