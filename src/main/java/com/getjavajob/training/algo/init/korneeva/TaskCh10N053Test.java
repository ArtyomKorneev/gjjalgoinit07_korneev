package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh10N053.turnSequence;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 11.08.16.
 */
public class TaskCh10N053Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh10N053Test.test", "54234", turnSequence(new int[]{4, 23, 54}, 3));
    }
}
