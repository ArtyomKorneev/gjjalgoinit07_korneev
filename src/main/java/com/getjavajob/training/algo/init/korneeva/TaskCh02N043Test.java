package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh02N043.numberIsDevidedByTheNumber;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 09.08.16.
 */
public class TaskCh02N043Test {
    public static void main(String[] args) {
        testDivided();
        testNotDivided();
    }

    private static void testDivided() {
        int result = numberIsDevidedByTheNumber(5, 15);
        assertEquals("TaskCh02N043Test", 1, result);
    }

    private static void testNotDivided() {
        int result = numberIsDevidedByTheNumber(5, 4);
        assertEquals("TaskCh02N043Test", 5, result);
    }
}
