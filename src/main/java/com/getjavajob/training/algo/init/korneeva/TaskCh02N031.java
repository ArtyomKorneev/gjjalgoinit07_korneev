package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 19.07.16.
 */
public class TaskCh02N031 {
    public static void main(String[] args) {
        System.out.println("Enter the number from 100 to 999");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int result = moveFigures(number);
        System.out.println(result);
    }

    public static int moveFigures(int number) {
        int a = number / 100;
        int b = number % 100 / 10;
        int c = number % 10;
        return a * 100 + c * 10 + b;
    }
}
