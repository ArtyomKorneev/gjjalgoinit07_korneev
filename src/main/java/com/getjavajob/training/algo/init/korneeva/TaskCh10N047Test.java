package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh10N047.fibonacciCalculation;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 11.08.16.
 */
public class TaskCh10N047Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh10N047Test.test", 8, fibonacciCalculation(5));
    }
}
