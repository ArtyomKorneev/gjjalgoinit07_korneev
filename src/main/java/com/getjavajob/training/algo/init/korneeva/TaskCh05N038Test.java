package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh05N038.getDistanceFromHome;
import static com.getjavajob.training.algo.init.korneeva.TaskCh05N038.getTotalDistance;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 09.08.16.
 */
public class TaskCh05N038Test {
    public static void main(String[] args) {
        test1();
        test2();
    }

    private static void test1() {
        assertEquals("TaskCh05N038Test.test1", 0.688172179310195, getDistanceFromHome());
    }

    private static void test2() {
        assertEquals("TaskCh05N038Test.test1", 5.187377517639621, getTotalDistance());
    }
}
