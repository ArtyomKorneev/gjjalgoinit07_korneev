package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh04N106.determineTheSeason;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 09.08.16.
 */
public class TaskCh04N106Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh04N106Test.test", "spring", determineTheSeason(5));
    }
}
