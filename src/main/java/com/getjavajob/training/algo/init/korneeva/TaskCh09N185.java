package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 27.07.16.
 */
public class TaskCh09N185 {
    public static void main(String[] args) {
        Search search = new Search();
        search.parse();
    }
}

class Stack {
    private int stck[];
    private int top;

    Stack(int size) {
        stck = new int[size];
        top = -1;
    }

    void push(int item) {
        if (top == stck.length - 1) {
            System.out.println("Stack is full");
        } else {
            stck[++top] = item;
        }
    }

    int pop() {
        if (top < 0) {
            System.out.println("Stack is empty");
            return 0;
        } else {
            return stck[top--];
        }
    }

    boolean empty() {
        return (top == -1);
    }

    public int getTop() {
        return top + 1;
    }
}

class Search {
    void parse() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the mathematical expression:");
        String expression = scanner.nextLine();
        Stack myStack = new Stack(expression.length());
        char[] chArray = expression.toCharArray();
        boolean extension = true;

        outer:
        for (int i = 0; i < chArray.length; i++) {
            switch (chArray[i]) {
                case '(':
                    myStack.push(chArray[i]);
                    break;
                case ')':
                    if (!myStack.empty()) {
                        myStack.pop();
                    } else {
                        System.out.println("No. Extra right parentheses on position " + (i + 1));
                        extension = false;
                        break outer;
                    }
                    break;
                default:
                    break;
            }
        }
        if (!myStack.empty()) {
            System.out.println("No. The quantity of extra left parentheses are equal to " + myStack.getTop());
        } else if (myStack.empty() & extension) {
            System.out.println("Yes");
        }
    }
}