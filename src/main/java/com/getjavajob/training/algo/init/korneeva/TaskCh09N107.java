package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 27.07.16.
 */
public class TaskCh09N107 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the word:");
        String word = scanner.nextLine();
        System.out.println(change(word));
    }

    public static String change(String word) {
        int begin = word.indexOf("a");
        int end = word.lastIndexOf("o");
        StringBuilder newWord = new StringBuilder(word);
        if (begin >= 0 && end >= 0) {
            newWord.setCharAt(begin, 'o');
            newWord.setCharAt(end, 'a');
            return String.valueOf(newWord);
        } else {
            return "error";
        }
    }
}
