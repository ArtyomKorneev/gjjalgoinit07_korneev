package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh12N234.deleteColumn;
import static com.getjavajob.training.algo.init.korneeva.TaskCh12N234.deleteRow;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 14.08.16.
 */
public class TaskCh12N234Test {
    public static void main(String[] args) {
        test1();
        test2();
    }

    private static void test1() {
        int[][] a = {
                {6, 7, 8, 9, 10},
                {11, 12, 13, 14, 15},
                {16, 17, 18, 19, 20},
                {21, 22, 23, 24, 25},
                {0, 0, 0, 0, 0}
        };
        assertEquals("TaskCh12N234Test.test", a, deleteRow(1));
    }

    private static void test2() {
        int[][] a = {
                {2, 3, 4, 5, 0},
                {7, 8, 9, 10, 0},
                {12, 13, 14, 15, 0},
                {17, 18, 19, 20, 0},
                {22, 23, 24, 25, 0}
        };
        assertEquals("TaskCh12N234Test.test", a, deleteColumn(1));
    }
}
