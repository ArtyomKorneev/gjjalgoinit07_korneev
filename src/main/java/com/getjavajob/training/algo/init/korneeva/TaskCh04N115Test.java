package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh04N115.getAnimal;
import static com.getjavajob.training.algo.init.korneeva.TaskCh04N115.getColor;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 09.08.16.
 */
public class TaskCh04N115Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        String result = getAnimal(1984) + ", " + getColor(1984);
        assertEquals("TaskCh04N115Test.test", "Rat, green", result);
    }
}
