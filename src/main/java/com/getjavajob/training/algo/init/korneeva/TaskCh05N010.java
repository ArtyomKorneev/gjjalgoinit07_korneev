package com.getjavajob.training.algo.init.korneeva;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by ZinZaga on 21.07.16.
 */
public class TaskCh05N010 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter course of dollar:");
        int course = scanner.nextInt();
        System.out.println(Arrays.toString(convertToRubles(course)));
    }

    public static int[] convertToRubles(int course) {
        int[] rub = new int[20];
        for (int i = 0; i < rub.length; i++) {
            rub[i] = i + 1;
        }

        for (int i = 0; i < rub.length; i++) {
            rub[i] *= course;
        }
        return rub;
    }
}
