package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh12N028.fillSnake;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 14.08.16.
 */
public class TaskCh12N028Test {
    public static void main(String[] args) {
        test1();
    }

    private static void test1() {
        int[][] a = {
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9}
        };
        assertEquals("TaskCh12N028Test.test", a, fillSnake(new int[5][5], 5));
    }
}
