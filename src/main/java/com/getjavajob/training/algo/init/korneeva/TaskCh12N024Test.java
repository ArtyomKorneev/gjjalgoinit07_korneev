package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh12N024.exampleA;
import static com.getjavajob.training.algo.init.korneeva.TaskCh12N024.exampleB;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 12.08.16.
 */
public class TaskCh12N024Test {
    public static void main(String[] args) {
        test1();
        test2();
    }

    private static void test1() {
        int[][] a = {
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252},
        };
        assertEquals("TaskCh12N024Test.test", a, exampleA(new int[6][6]));
    }

    private static void test2() {
        int[][] a = {
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5},
        };
        assertEquals("TaskCh12N024Test.test", a, exampleB(a));
    }
}
