package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh10N044.calculateTheDigitalRoot;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 10.08.16.
 */
public class TaskCh10N044Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh10N044Test.test", 5, calculateTheDigitalRoot(122));
    }
}
