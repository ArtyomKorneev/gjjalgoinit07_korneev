package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 20.07.16.
 */
public class TaskCh04N115 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the year:");
        int year = scanner.nextInt();
        String result = getAnimal(year) + ", " + getColor(year);
        System.out.println(result);
        int begin = 1984;
        int cycle = 60;
        while (year < begin) {
            year += cycle;
        }
    }

    public static String getAnimal(int year) {
        int animal = (year - 1984) % 12;
        String beast = null;
        switch (animal) {
            case 0:
                beast = "Rat";
                break;
            case 1:
                beast = "Cow";
                break;
            case 2:
                beast = "Tiger";
                break;
            case 3:
                beast = "Hare";
                break;
            case 4:
                beast = "Dragon";
                break;
            case 5:
                beast = "Snake";
                break;
            case 6:
                beast = "Horse";
                break;
            case 7:
                beast = "Sheep";
                break;
            case 8:
                beast = "Monkey";
                break;
            case 9:
                beast = "Cock";
                break;
            case 10:
                beast = "Dog";
                break;
            case 11:
                beast = "Pig";
                break;
        }
        return beast;
    }

    public static String getColor(int year) {
        int color = (year - 1984) % 10;
        String hue = null;
        switch (color) {
            case 0:
            case 1:
                hue = "green";
                break;
            case 2:
            case 3:
                hue = "red";
                break;
            case 4:
            case 5:
                hue = "yellow";
                break;
            case 6:
            case 7:
                hue = "white";
                break;
            case 8:
            case 9:
                hue = "black";
                break;
        }
        return hue;
    }
}
