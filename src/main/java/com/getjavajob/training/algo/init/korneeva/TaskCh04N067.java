package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 20.07.16.
 */
public class TaskCh04N067 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the day of year from 1 to 365:");
        int day = scanner.nextInt();
        System.out.println(weekendsOrWorkdays(day));
    }

    public static String weekendsOrWorkdays(int day) {
        if (day % 7 == 0 || day % 7 == 6) {
            return "Weekend";
        } else {
            return "Workday";
        }
    }
}
