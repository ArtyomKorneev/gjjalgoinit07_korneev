package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh10N056.isSimple;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 11.08.16.
 */
public class TaskCh10N056Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh10N056Test.test", true, isSimple(17, 2));
    }
}
