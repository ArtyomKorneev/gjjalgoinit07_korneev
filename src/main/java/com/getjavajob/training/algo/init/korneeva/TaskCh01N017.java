package com.getjavajob.training.algo.init.korneeva;

import static java.lang.Math.abs;
import static java.lang.Math.pow;

/**
 * Created by ZinZaga on 15.07.16.
 */
public class TaskCh01N017 {
    public static void main(String[] args) {
        int a = 2;
        int b = 3;
        int c = 4;
        int x = 3;

        double o = Math.sqrt(1 - Math.sin(2));
        double p = 1 / Math.sqrt(a * pow(x, 2) + b * x + c);
        double r = (Math.sqrt(x + 1) + Math.sqrt(x - 1)) / (2 * Math.sqrt(x));
        double s = abs(x) + abs(x + 1);
    }
}
