package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh09N017.compareFirstAndLastLetter;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 10.08.16.
 */
public class TaskCh09N017Test {
    public static void main(String[] args) {
        testPositive();
        testNegative();
    }

    private static void testPositive() {
        assertEquals("TaskCh09N017Test.testEven", true, compareFirstAndLastLetter("abracadabra"));
    }

    private static void testNegative() {
        assertEquals("TaskCh09N017Test.testOdd", false, compareFirstAndLastLetter("bird"));
    }
}
