package com.getjavajob.training.algo.init.korneeva;

import java.util.Arrays;

/**
 * Created by ZinZaga on 12.08.16.
 */
public class TaskCh12N025 {
    public static void main(String[] args) {
        Arrays.toString(exampleA());
        Arrays.toString(exampleB());
        Arrays.toString(exampleV());
        Arrays.toString(exampleG());
        Arrays.toString(exampleD());
        Arrays.toString(exampleE());
        Arrays.toString(exampleJ());
        Arrays.toString(exampleZ());
        Arrays.toString(exampleN());
        Arrays.toString(exampleK());
        Arrays.toString(exampleL());
        Arrays.toString(exampleM());
        Arrays.toString(exampleH());
        Arrays.toString(exampleO());
        Arrays.toString(exampleP());
        Arrays.toString(exampleR());

    }

    public static int[][] toDisplay(int[][] a) {
        for (int[] i : a) {
            for (int j : i) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
        return a;
    }

    public static int[][] exampleA() {
        int[][] a = new int[12][10];
        int k = 1;
        for (int i = 0; i < 12; i++) {
            for (int j = 0; j < 10; j++) {
                a[i][j] = k++;
            }
        }
        toDisplay(a);
        return a;
    }

    public static int[][] exampleB() {
        int[][] a = new int[12][10];
        int k = 1;
        for (int j = 0; j < 10; j++) {
            for (int i = 0; i < 12; i++) {
                a[i][j] = k++;
            }
        }
        toDisplay(a);
        return a;
    }

    public static int[][] exampleV() {
        int[][] a = new int[12][10];
        int k = 1;
        for (int i = 0; i < 12; i++) {
            for (int j = 9; j >= 0; j--) {
                a[i][j] = k++;
            }
        }
        toDisplay(a);
        return a;
    }

    public static int[][] exampleG() {
        int[][] a = new int[12][10];
        int k = 1;
        for (int j = 0; j < 10; j++) {
            for (int i = 11; i >= 0; i--) {
                a[i][j] = k++;
            }
        }
        toDisplay(a);
        return a;
    }

    public static int[][] exampleD() {
        int[][] a = new int[10][12];
        int k = 1;
        for (int i = 0; i < a.length; i++) {
            if (i % 2 == 0) {
                for (int j = 0; j < a[i].length; j++) {
                    a[i][j] = k++;
                }
            } else {
                for (int j = a[i].length - 1; j >= 0; j--) {
                    a[i][j] = k++;
                }
            }
        }
        toDisplay(a);
        return a;
    }

    public static int[][] exampleE() {
        int[][] a = new int[12][10];
        int k = 1;
        for (int j = 0; j < a[j].length; j++) {
            if (j % 2 == 0) {
                for (int i = 0; i <= a.length - 1; i++) {
                    a[i][j] = k++;
                }
            } else {
                for (int i = a.length - 1; i >= 0; i--) {
                    a[i][j] = k++;
                }
            }
        }
        toDisplay(a);
        return a;
    }

    public static int[][] exampleJ() {
        int[][] a = new int[12][10];
        int k = 1;
        for (int i = 11; i >= 0; i--) {
            for (int j = 0; j < 10; j++) {
                a[i][j] = k++;
            }
        }
        toDisplay(a);
        return a;
    }

    public static int[][] exampleZ() {
        int[][] a = new int[12][10];
        int k = 1;
        for (int j = a[0].length - 1; j >= 0; j--) {
            for (int i = 0; i < a.length; i++) {
                a[i][j] = k++;
            }
        }
        toDisplay(a);
        return a;
    }

    public static int[][] exampleN() {
        int[][] a = new int[12][10];
        int k = 1;
        for (int i = 11; i >= 0; i--) {
            for (int j = 9; j >= 0; j--) {
                a[i][j] = k++;
            }
        }
        toDisplay(a);
        return a;
    }

    public static int[][] exampleK() {
        int[][] a = new int[12][10];
        int k = 1;
        for (int j = 9; j >= 0; j--) {
            for (int i = 11; i >= 0; i--) {
                a[i][j] = k++;
            }
        }
        toDisplay(a);
        return a;
    }

    public static int[][] exampleL() {
        int[][] a = new int[12][10];
        int k = 1;
        for (int i = 11; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = 9; j >= 0; j--) {
                    a[i][j] = k++;
                }
            } else {
                for (int j = 0; j < 10; j++) {
                    a[i][j] = k++;
                }
            }
        }
        toDisplay(a);
        return a;
    }

    public static int[][] exampleM() {
        int[][] a = new int[12][10];
        int k = 1;
        for (int i = 0; i < 12; i++) {
            if (i % 2 == 0) {
                for (int j = 9; j >= 0; j--) {
                    a[i][j] = k++;
                }
            } else {
                for (int j = 0; j < 10; j++) {
                    a[i][j] = k++;
                }
            }
        }
        toDisplay(a);
        return a;
    }

    public static int[][] exampleH() {
        int[][] a = new int[12][10];
        int k = 1;
        for (int j = 9; j >= 0; j--) {
            if (j % 2 == 0) {
                for (int i = 11; i >= 0; i--) {
                    a[i][j] = k++;
                }
            } else {
                for (int i = 0; i < 12; i++) {

                    a[i][j] = k++;
                }
            }
        }
        toDisplay(a);
        return a;
    }

    public static int[][] exampleO() {
        int[][] a = new int[12][10];
        int k = 1;
        for (int j = 0; j < 10; j++) {
            if (j % 2 == 0) {
                for (int i = 11; i >= 0; i--) {
                    a[i][j] = k++;
                }
            } else {
                for (int i = 0; i < 12; i++) {
                    a[i][j] = k++;
                }
            }
        }
        toDisplay(a);
        return a;
    }

    public static int[][] exampleP() {
        int[][] a = new int[12][10];
        int k = 1;
        for (int i = 11; i >= 0; i--) {
            if (i % 2 == 0) {
                for (int j = 0; j <= 9; j++) {
                    a[i][j] = k++;
                }
            } else {
                for (int j = 9; j >= 0; j--) {
                    a[i][j] = k++;
                }
            }
        }
        toDisplay(a);
        return a;
    }

    public static int[][] exampleR() {
        int[][] a = new int[12][10];
        int k = 1;
        for (int j = 9; j >= 0; j--) {
            if (j % 2 == 0) {
                for (int i = 0; i <= 11; i++) {
                    a[i][j] = k++;
                }
            } else {
                for (int i = 11; i >= 0; i--) {
                    a[i][j] = k++;
                }
            }
        }
        toDisplay(a);
        return a;
    }
}
