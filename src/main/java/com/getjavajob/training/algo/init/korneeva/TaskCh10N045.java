package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 03.08.16.
 */
public class TaskCh10N045 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the first number:");
        int first = scanner.nextInt();
        System.out.println("Enter the step:");
        int step = scanner.nextInt();
        System.out.println("Enter the n:");
        int n = scanner.nextInt();
        System.out.println(arithmeticProgression(first, step, n));
        System.out.println(sumOfThree(first, step, n));
    }

    public static int arithmeticProgression(int first, int step, int n) {
        if (n == 1) {
            return first;
        } else {
            return arithmeticProgression(first + step, step, --n);
        }
    }

    public static int sumOfThree(int first, int step, int n) {
        if (n == 1) {
            return first;
        } else {
            return sumOfThree(first + step, step, --n) + first;
        }
    }
}