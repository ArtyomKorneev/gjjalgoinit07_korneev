package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 20.07.16.
 */
public class TaskCh04N036 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the minutes:");
        int minutes = scanner.nextInt();
        System.out.println(trafficLight(minutes));
    }

    public static String trafficLight(int minutes) {
        return minutes % 5 < 3 ? "green" : "red";
    }
}
