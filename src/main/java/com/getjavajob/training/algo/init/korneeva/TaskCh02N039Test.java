package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh02N039.angleClockwise;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 09.08.16.
 */
public class TaskCh02N039Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        double result = angleClockwise(18, 0, 0);
        assertEquals("TaskCh02N039Test", 180, result);
    }
}
