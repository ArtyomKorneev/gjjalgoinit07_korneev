package com.getjavajob.training.algo.init.korneeva;

/**
 * Created by ZinZaga on 21.07.16.
 */
public class TaskCh05N038 {
    public static void main(String[] args) {
        System.out.println("Distance from home " + getDistanceFromHome());
        System.out.println("Total distance " + getTotalDistance());
    }

    public static double getDistanceFromHome() {
        double strange = 0;
        for (double i = 1; i < 101; i++) {
            strange += (2 * (i % 2) - 1) / i;
        }
        return strange;
    }

    public static double getTotalDistance() {
        double total = 0;
        for (double i = 1; i < 101; i++) {
            total += 1 / i;
        }
        return total;
    }
}
