package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh12N063.averageNumStudents;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 14.08.16.
 */
public class TaskCh12N063Test {
    public static void main(String[] args) {
        test1();
    }

    private static void test1() {
        assertEquals("TaskCh12N063Test.test", 2.5, averageNumStudents(new int[11][4], 0, 1));
    }
}
