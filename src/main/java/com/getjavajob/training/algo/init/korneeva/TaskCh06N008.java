package com.getjavajob.training.algo.init.korneeva;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by ZinZaga on 21.07.16.
 */
public class TaskCh06N008 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number:");
        int n = scanner.nextInt();
        System.out.println(Arrays.toString(getUpTo(n)));
    }

    public static double[] getUpTo(int n) {
        int start = 1;
        while (Math.pow(start, 2) < n) {
            start++;
        }
        double[] square = new double[start - 1];
        for (int i = 0; i < start - 1; i++) {
            square[i] = Math.pow(i + 1, 2);
        }
        return square;
    }
}
