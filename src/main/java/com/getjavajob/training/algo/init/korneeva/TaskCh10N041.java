package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 03.08.16.
 */
public class TaskCh10N041 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number:");
        int number = scanner.nextInt();
        System.out.println(factorialCalculation(number));
    }

    public static int factorialCalculation(int number) {
        if (number == 1) {
            return 1;
        } else {
            return factorialCalculation(number - 1) * number;
        }
    }
}