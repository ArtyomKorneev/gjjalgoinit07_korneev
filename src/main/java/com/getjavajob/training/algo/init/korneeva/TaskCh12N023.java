package com.getjavajob.training.algo.init.korneeva;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by ZinZaga on 12.08.16.
 */
public class TaskCh12N023 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the odd number of rows and columns:");
        int side = scanner.nextInt();
        int a[][] = new int[side][side];
        System.out.println("Result of the example a:");
        Arrays.toString(exampleA(a));
        System.out.println("Result of the example b:");
        Arrays.toString(exampleB(a));
        System.out.println("Result of the example c:");
        Arrays.toString(exampleC(a));
    }

    public static int[][] exampleA(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                if (i == j || i + j == a.length - 1) {
                    a[i][j] = 1;
                } else {
                    a[i][j] = 0;
                }
            }
        }
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
        return a;
    }

    public static int[][] exampleB(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                if (i == j || i + j == a.length - 1 || j == (a.length - 1) / 2 || i == (a.length - 1) / 2) {
                    a[i][j] = 1;
                } else {
                    a[i][j] = 0;
                }
            }
        }
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
        return a;
    }

    public static int[][] exampleC(int[][] a) {
        for (int i = 0; i < a.length / 2 + 1; i++) {
            for (int j = 0; j < a[i].length - i; j++) {
                if (j < i || j >= (a[i].length - i)) {
                    a[i][j] = 0;
                } else {
                    a[i][j] = 1;
                }
            }
        }
        for (int i = a.length - 1; i >= a.length / 2; i--) {
            for (int j = 0; j < a[i].length; j++) {
                if ((j < (a[i].length - 1 - i)) || (j > i)) {
                    a[i][j] = 0;
                } else {
                    a[i][j] = 1;
                }
            }
        }
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
        return a;
    }
}
