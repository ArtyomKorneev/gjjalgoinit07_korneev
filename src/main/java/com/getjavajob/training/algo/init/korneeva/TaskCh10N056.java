package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 11.08.16.
 */
public class TaskCh10N056 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number:");
        int number = scanner.nextInt();
        System.out.println("Enter number 2:");
        int two = scanner.nextInt();
        System.out.println(isSimple(number, two));
    }

    public static boolean isSimple(int number, int two) {
        if (number == 1) {
            return false;
        } else if (number == 2) {
            return true;
        } else if (number % two == 0) {
            return false;
        } else if (two <= Math.sqrt(number)) {
            return isSimple(number, two + 1);
        } else {
            return true;
        }
    }
}
