package com.getjavajob.training.algo.init.korneeva;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by ZinZaga on 22.07.16.
 */
public class TaskCh13N012 {
    public static void main(String[] args) {
        Database data = new Database();
        data.addEmployees(new Employee("Ivanov", "Ivan", "Ivanovich", "Pushkina 8", 1, 1995));
        data.addEmployees(new Employee("Petrov", "Semen", "Kolotushkina 9", 2, 1996));
        data.addEmployees(new Employee("Alba", "Jessica", "Petrovna", "District 7", 3, 1997));
        data.addEmployees(new Employee("Korneev", "Artyom", "Aleksandrovich", "Ostozhenka 17", 4, 1998));
        data.addEmployees(new Employee("Sidorov", "Vladimir", "Sergeevich", "Achunova 6", 5, 1999));
        data.addEmployees(new Employee("Drozdov", "Andrew", "Ivanovich", "District 5", 6, 2000));
        data.addEmployees(new Employee("Volya", "Pavel", "Nikolaevich", "5 avenue", 7, 2001));
        data.addEmployees(new Employee("Egorov", "Andrew", "Yurevich", "Sacharova, 32", 8, 2002));
        data.addEmployees(new Employee("Gilev", "Kirill", "Semenovich", "Pushkina 8", 9, 2003));
        data.addEmployees(new Employee("Pushkin", "Alexander", "Sergeevich", "Arbatskaya 7", 10, 2000));
        data.addEmployees(new Employee("Lermontov", "Mihail", "Yurevich", "Tverskaya 5", 11, 2001));
        data.addEmployees(new Employee("Martin", "Lutter", "King", "Achunova 6", 12, 2002));
        data.addEmployees(new Employee("Suvorov", "Alexander", "Vasilevich", "District 12", 1, 2003));
        data.addEmployees(new Employee("Davidov", "Denis", "5 avenue", 2, 2004));
        data.addEmployees(new Employee("Lenin", "Vladimir", "Ilich", "Sacharova, 32", 3, 2005));
        data.addEmployees(new Employee("Stalin", "Iosif", "Vissarionovich", "Kirova 76", 4, 2006));
        data.addEmployees(new Employee("Marks", "Carl", "Camala 93", 5, 2007));
        data.addEmployees(new Employee("Kovalevskaya", "Sofia", "Koroleva 100", 6, 2008));
        data.addEmployees(new Employee("Kulikova", "Lubov", "Ivanovna", "Glushko 47", 7, 2009));
        data.addEmployees(new Employee("Dragunova", "Mariya", "Andreevna", "Zinina 4", 8, 2010));
        List<Employee> employeesByYear = data.searchEmployeesByYear(8, 2016, 21);
        data.printEmployees(employeesByYear);
        List<Employee> employeesBySubstring = data.searchEmployeesBySubstring("lenin");
        data.printEmployees(employeesBySubstring);
    }
}

class Employee {
    private String name;
    private String middleName;
    private String surname;
    private String address;
    private int monthOfEmployment;
    private int yearOfEmployment;

    public Employee(String name, String surname, String address, int monthOfEmployment, int yearOfEmployment) {
        this(name, "", surname, address, monthOfEmployment, yearOfEmployment);
    }

    public Employee(String name, String middleName, String surname, String address, int monthOfEmployment, int yearOfEmployment) {
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.monthOfEmployment = monthOfEmployment;
        this.yearOfEmployment = yearOfEmployment;
        this.middleName = middleName;
    }

    String getName() {
        return name;
    }

    String getMiddleName() {
        return middleName;
    }

    String getSurname() {
        return surname;
    }

    int getNumOfYearsWorked(int month, int year) {
        int exprMonths = year * 12 + month;
        int exprMonthsWorked = yearOfEmployment * 12 + monthOfEmployment;
        return (exprMonths - exprMonthsWorked) / 12;
    }

    @Override
    public String toString() {
        if (!Objects.equals(middleName, "")) {
            return name +
                    ", " +
                    middleName +
                    ", " +
                    surname +
                    ", " +
                    address +
                    ", " +
                    monthOfEmployment +
                    ", " +
                    yearOfEmployment;
        } else {
            return name +
                    ", " +
                    surname +
                    ", " +
                    address +
                    ", " +
                    monthOfEmployment +
                    ", " +
                    yearOfEmployment;
        }
    }
}

class Database {
    private List<Employee> employees;

    public Database() {
        employees = new ArrayList<>();
    }

    void addEmployees(Employee employee) {
        employees.add(employee);
    }

    List<Employee> searchEmployeesBySubstring(String substring) {
        List<Employee> exprEmployees = new ArrayList<>();
        for (Employee employee : employees) {
            String name = employee.getName().toLowerCase();
            String surname = employee.getSurname().toLowerCase();
            String middleName = employee.getMiddleName().toLowerCase();
            if (name.contains(substring.toLowerCase()) || middleName.contains(substring.toLowerCase()) || surname.contains(substring.toLowerCase())) {
                exprEmployees.add(employee);
            }
        }
        return exprEmployees;
    }

    List<Employee> searchEmployeesByYear(int month, int year, int numOfYears) {
        List<Employee> exprEmployees = new ArrayList<>();
        for (Employee employee : employees) {
            if (employee.getNumOfYearsWorked(month, year) >= numOfYears) {
                exprEmployees.add(employee);
            }
        }
        return exprEmployees;
    }

    void printEmployees(List<Employee> employees) {
        for (Employee employee : employees) {
            System.out.println(employee.toString());
        }
    }
}
