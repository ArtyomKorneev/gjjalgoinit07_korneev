package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 03.08.16.
 */
public class TaskCh10N042 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number:");
        int number = scanner.nextInt();
        System.out.println("Enter the degree:");
        int degree = scanner.nextInt();
        System.out.println(involution(number, degree));
    }

    public static double involution(double number, int degree) {
        if (degree == 0) {
            return 1;
        } else if (degree == 1) {
            return number;
        } else {
            degree--;
            return number * involution(number, degree);
        }
    }
}