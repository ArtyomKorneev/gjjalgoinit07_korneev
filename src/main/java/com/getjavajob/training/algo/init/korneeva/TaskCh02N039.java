package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 19.07.16.
 */
public class TaskCh02N039 {
    public static void main(String[] args) {
        System.out.println("Enter hours from 0 to 23, minutes from 0 to 59 and seconds from 0 to 59");
        Scanner scanner = new Scanner(System.in);
        double hours = scanner.nextInt();
        double minutes = scanner.nextInt();
        double seconds = scanner.nextInt();
        double result = angleClockwise(hours, minutes, seconds);
        System.out.println("angle=" + result);
    }

    public static double angleClockwise(double hours, double minutes, double seconds) {
        double h = hours % 12;
        return h * 30 + minutes * 0.5 + seconds * (0.5 / 60);
    }
}
