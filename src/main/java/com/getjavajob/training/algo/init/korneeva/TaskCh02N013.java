package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 19.07.16.
 */
public class TaskCh02N013 {
    public static void main(String[] args) {
        System.out.println("Enter the number from 100 to 200");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int result = fromRightToLeft(number);
        System.out.println(result);
    }

    public static int fromRightToLeft(int number) {
        return number % 10 * 100 + number % 100 / 10 * 10 + number / 100;
    }
}