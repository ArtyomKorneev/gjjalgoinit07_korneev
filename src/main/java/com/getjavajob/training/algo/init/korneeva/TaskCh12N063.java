package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 14.08.16.
 */
public class TaskCh12N063 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[][] numberOfPupils = new int[11][4];
        double pupils = 0;
        System.out.println("Enter the number of classes:");
        int number = scanner.nextInt();
        System.out.println(averageNumStudents(numberOfPupils, pupils, number));
    }

    public static double averageNumStudents(int[][] numberOfPupils, double pupils, int number) {
        int k = 1;
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 4; j++) {
                numberOfPupils[i][j] = k++;
            }
        }
        for (int i = 0; i < 11; i++) {
            if (i + 1 == number) {
                for (int j = 0; j < 4; j++) {
                    pupils += numberOfPupils[i][j];
                }
            }
        }
        return pupils / 4;
    }
}
