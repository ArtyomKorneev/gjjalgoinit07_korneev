package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh04N036.trafficLight;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 09.08.16.
 */
public class TaskCh04N036Test {
    public static void main(String[] args) {
        test1();
        test2();
    }

    private static void test1() {
        assertEquals("TaskCh04N036Test.test1", "red", trafficLight(3));
    }

    private static void test2() {
        assertEquals("TaskCh04N036Test.test2", "green", trafficLight(5));
    }
}
