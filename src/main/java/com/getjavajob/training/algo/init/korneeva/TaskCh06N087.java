package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 22.07.16.
 */
public class TaskCh06N087 {
    public static void main(String[] args) {
        Game game = new Game();
        game.play();
    }
}

class Team {
    String name;
    int score;
}

class Game {
    Scanner scanner = new Scanner(System.in);
    Team team1 = new Team();
    Team team2 = new Team();
    private boolean gameOver;

    public void setNameOfTeam1(String nameOfTeam1) {
        this.team1.name = nameOfTeam1;
    }

    public void setNameOfTeam2(String nameOfTeam2) {
        this.team2.name = nameOfTeam2;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public void setScoreOfTeam1(int scoreOfTeam1) {
        this.team1.score = scoreOfTeam1;
    }

    public void setScoreOfTeam2(int scoreOfTeam2) {
        this.team2.score = scoreOfTeam2;
    }

    void play() {
        team1.score = 0;
        team2.score = 0;
        System.out.println("Enter team #1:");
        team1.name = scanner.nextLine();
        System.out.println("Enter team #2:");
        team2.name = scanner.nextLine();
        do {
            System.out.println("Enter team to score (1 or 2 or 0 to finish game):");
            int team = scanner.nextInt();
            if (team == 0) {
                gameOver = true;
                System.out.println(result());
            } else if (team > 0 && team < 3) {
                System.out.println("Enter score (1 or 2 or 3):");
                int goal = scanner.nextInt();
                if (goal == 1 || goal == 2 || goal == 3) {
                    if (team == 1) {
                        team1.score += goal;
                    } else {
                        team2.score += goal;
                    }
                    System.out.println(score());
                } else {
                    System.out.println("Invalid score number! Must be 1 or 2 or 3");
                }
            } else {
                System.out.println("Invalid team number! Must be 1 or 2 or 0 to finish game");
            }
        } while (!gameOver);
    }

    String score() {
        return "score: " + team1.score + " - " + team2.score;
    }

    String result() {
        if (!gameOver) {
            return team1.score + " - " + team2.score;
        } else {
            String winner;
            if (team1.score > team2.score) {
                winner = team1.name;
                return team1.score + " - " + team2.score + "\nWinner: " + winner;
            } else if (team1.score < team2.score) {
                winner = team2.name;
                return team1.score + " - " + team2.score + "\nWinner: " + winner;
            } else {
                winner = "Draw";
                return team1.score + " - " + team2.score + "\n" + winner;
            }
        }
    }
}