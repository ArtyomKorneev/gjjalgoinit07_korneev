package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh11N245.fromMinusToPlus;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 12.08.16.
 */
public class TaskCh11N245Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh11N245Test.test", new int[]{-72, -89, -45, 4, 5}, fromMinusToPlus(new int[]{5, -72, 4, -89, -45}));
    }
}
