package com.getjavajob.training.algo.init.korneeva;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by ZinZaga on 11.08.16.
 */
public class TaskCh11N158 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("How many items will be in the array?");
        int size = scanner.nextInt();
        int[] a = new int[size];
        System.out.println("Enter the value of the elements:");
        for (int i = 0; i < a.length; i++) {
            a[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(removeDuplicates(a)));
    }

    public static int[] removeDuplicates(int[] a) {
        for (int i = 0; i < a.length - 1; i++) {
            for (int j = a.length - 1; j >= 0; j--) {
                if (a[i] == a[j]) {
                    int temp = a[i];
                    a[j] = 0;
                    a[i] = temp;
                }
            }
        }
        int size = a.length;
        for (int i = 0; i < size; i++) {
            if (a[i] == 0) {
                for (int j = i; j < size - 1; j++) {
                    a[j] = a[j + 1];
                }
                size--;
                a[size] = 0;
            }
        }
        return a;
    }
}
