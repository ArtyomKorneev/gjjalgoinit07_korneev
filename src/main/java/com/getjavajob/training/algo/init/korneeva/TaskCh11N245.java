package com.getjavajob.training.algo.init.korneeva;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by ZinZaga on 12.08.16.
 */
public class TaskCh11N245 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("How many items will be in the array?");
        int size = scanner.nextInt();
        int[] a = new int[size];
        System.out.println("Enter the value of the elements:");
        for (int i = 0; i < a.length; i++) {
            a[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(fromMinusToPlus(a)));
    }

    public static int[] fromMinusToPlus(int[] a) {
        int size = a.length;
        int[] b = new int[size];
        for (int i = 0, j = 0, k = size - 1; i < size; i++) {
            if (a[i] < 0) {
                b[j++] = a[i];
            } else {
                b[k--] = a[i];
            }
        }
        return b;
    }
}
