package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 04.08.16.
 */
public class TaskCh10N049 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of elements:");
        int elements = scanner.nextInt();
        System.out.println("Enter the numbers:");
        int[] arr = new int[elements];
        for (int i = 0; i < arr.length; i++) {
            int j = scanner.nextInt();
            arr[i] = j;
        }
        int start = 0;
        int max = findMax(arr, start);
        System.out.println(findIndex(arr, start, max));
    }

    public static int findIndex(int[] arr, int start, int max) {
        if (start < arr.length) {
            int next = findIndex(arr, start + 1, max);
            if (arr[start] == max) {
                return start;
            } else {
                return next;
            }
        } else {
            return start - 1;
        }
    }

    public static int findMax(int[] arr, int start) {
        if (start < arr.length) {
            int next = findMax(arr, start + 1);
            if (arr[start] > next) {
                return arr[start];
            } else {
                return next;
            }
        } else {
            return arr[start - 1];
        }
    }
}
