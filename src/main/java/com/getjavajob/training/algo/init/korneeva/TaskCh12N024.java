package com.getjavajob.training.algo.init.korneeva;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by ZinZaga on 12.08.16.
 */
public class TaskCh12N024 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of rows and columns:");
        int side = scanner.nextInt();
        int[][] a = new int[side][side];
        System.out.println("Result of the example a:");
        Arrays.toString(exampleA(a));
        System.out.println();
        Arrays.toString(exampleB(a));
    }

    public static int[][] exampleA(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                if (i == 0 || j == 0) {
                    a[i][j] = 1;
                } else if (i == 1 && j > 0) {
                    a[i][j] = j + 1;
                } else if (j == 1 && i > 0) {
                    a[i][j] = i + 1;
                } else if (i > 1) {
                    a[i][j] = a[i - 1][j] + a[i][j - 1];
                }
            }
        }
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
        return a;
    }

    public static int[][] exampleB(int[][] a) {
        int[] arr = {1, 2, 3, 4, 5, 6};
        for (int i = 0; i < arr.length; i++) {
            System.out.println(Arrays.toString(arr));
            int tmp = arr[0];
            System.arraycopy(arr, 1, arr, 0, arr.length - 1);
            arr[arr.length - 1] = tmp;
        }
        return a;
    }
}
