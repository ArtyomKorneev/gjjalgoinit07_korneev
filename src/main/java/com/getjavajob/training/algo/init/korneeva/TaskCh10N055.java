package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 24.08.16.
 */
public class TaskCh10N055 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number:");
        int number = scanner.nextInt();
        System.out.println("Enter scale of notation:");
        int notation = scanner.nextInt();
        System.out.println(notation == 2 ? decimalToBinary(number) : decimalToHexadecimal(number));
    }

    public static String decimalToBinary(int number) {
        return number == 0 ? "" : decimalToBinary(number / 2) + number % 2;
    }

    public static String decimalToHexadecimal(int number) {
        String hex = "0123456789ABCDEF";
        return number == 0 ? "" : decimalToHexadecimal(number / 16) + hex.charAt(number % 16);
    }
}
