package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 24.08.16.
 */
public class TaskCh06N087Test {
    public static void main(String[] args) {
        testResultFirstTeamWinner();
        testResultSecondTeamWinner();
        testResultDraw();
        testScore();
    }

    private static void testResultFirstTeamWinner() {
        Game game = new Game();
        game.setNameOfTeam1("First");
        game.setNameOfTeam2("Second");
        game.setGameOver(true);
        game.setScoreOfTeam1(1);
        assertEquals("TaskCh06N087Test.testResultFirstTeamWinner", "1 - 0\n" + "Winner: First", game.result());
    }

    private static void testResultSecondTeamWinner() {
        Game game = new Game();
        game.setNameOfTeam1("First");
        game.setNameOfTeam2("Second");
        game.setGameOver(true);
        game.setScoreOfTeam1(0);
        game.setScoreOfTeam2(3);
        assertEquals("TaskCh06N087Test.testResultSecondTeamWinner", "0 - 3\n" + "Winner: Second", game.result());
    }

    private static void testResultDraw() {
        Game game = new Game();
        game.setNameOfTeam1("First");
        game.setNameOfTeam2("Second");
        game.setGameOver(true);
        game.setScoreOfTeam1(1);
        game.setScoreOfTeam2(1);
        assertEquals("TaskCh06N087Test.testResultDraw", "1 - 1\n" + "Draw", game.result());
    }

    private static void testScore() {
        Game game = new Game();
        game.setNameOfTeam1("First");
        game.setNameOfTeam2("Second");
        game.setGameOver(false);
        game.setScoreOfTeam1(2);
        game.setScoreOfTeam2(0);
        assertEquals("TaskCh06N087Test.testScore", "score: 2 - 0", game.score());
    }

}
