package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh10N043.amountCalculation;
import static com.getjavajob.training.algo.init.korneeva.TaskCh10N043.calculationOfTheNumberOfDigits;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 10.08.16.
 */
public class TaskCh10N043Test {
    public static void main(String[] args) {
        test1();
        test2();
    }

    private static void test1() {
        assertEquals("TaskCh10N043Test.test", 21, amountCalculation(876));
    }

    private static void test2() {
        assertEquals("TaskCh10N043Test.test", 3, calculationOfTheNumberOfDigits(876));
    }
}
