package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 19.07.16.
 */
public class TaskCh04N033 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the number:");
        int a = in.nextInt();
        boolean even = isEven(a);
        System.out.println(even ? "even" : "odd");
    }

    public static boolean isEven(int a) {
        return a % 2 == 0;
    }
}

