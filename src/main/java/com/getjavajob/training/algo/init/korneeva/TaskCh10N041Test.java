package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh10N041.factorialCalculation;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 10.08.16.
 */
public class TaskCh10N041Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh10N041Test.test", 24, factorialCalculation(4));
    }
}
