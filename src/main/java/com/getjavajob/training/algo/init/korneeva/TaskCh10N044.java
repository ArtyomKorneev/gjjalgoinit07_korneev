package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 03.08.16.
 */
public class TaskCh10N044 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number:");
        int number = scanner.nextInt();
        System.out.println(calculateTheDigitalRoot(number));
    }

    public static int calculateTheDigitalRoot(int number) {
        int sum = 0;
        while (number != 0) {
            sum += number % 10;
            number = number / 10;
        }
        if (sum < 10) {
            return sum;
        } else {
            return calculateTheDigitalRoot(sum);
        }
    }
}