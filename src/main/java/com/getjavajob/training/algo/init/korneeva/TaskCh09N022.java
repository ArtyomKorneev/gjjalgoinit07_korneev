package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 27.07.16.
 */
public class TaskCh09N022 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the word:");
        String word = scanner.nextLine();
        System.out.println(halfDisplay(word));
    }

    public static String halfDisplay(String word) {
        int letters = word.length();
        int half = letters / 2;
        word = word.substring(0, half);
        return word;
    }
}
