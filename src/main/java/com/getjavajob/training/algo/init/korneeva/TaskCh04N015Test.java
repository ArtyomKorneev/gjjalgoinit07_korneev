package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh04N015.calculateAge;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh04N015Test {
    public static void main(String[] args) {
        test1();
        test2();
        test3();
    }

    private static void test1() {
        int result = calculateAge(12, 2014, 6, 1985);
        assertEquals("TaskCh04N015Test.test1", 29, result);
    }

    private static void test2() {
        int result = calculateAge(5, 2014, 6, 1985);
        assertEquals("TaskCh04N015Test.test2", 28, result);
    }

    private static void test3() {
        int result = calculateAge(6, 2014, 6, 1985);
        assertEquals("TaskCh04N015Test.test3", 29, result);
    }
}
