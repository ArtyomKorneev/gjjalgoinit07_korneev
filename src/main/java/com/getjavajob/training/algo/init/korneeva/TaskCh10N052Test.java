package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh10N052.numberInReverseOrder;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 11.08.16.
 */
public class TaskCh10N052Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh10N052Test.test", "7654321", numberInReverseOrder(1234567));
    }
}
