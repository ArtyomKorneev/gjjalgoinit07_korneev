package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh05N010.convertToRubles;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 09.08.16.
 */
public class TaskCh05N010Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh05N010Test.test", new int[]{45, 90, 135, 180, 225, 270, 315, 360, 405, 450, 495, 540, 585, 630, 675, 720, 765, 810, 855, 900}, convertToRubles(45));
    }
}
