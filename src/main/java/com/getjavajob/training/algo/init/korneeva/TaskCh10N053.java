package com.getjavajob.training.algo.init.korneeva;

/**
 * Created by ZinZaga on 11.08.16.
 */
public class TaskCh10N053 {
    public static void main(String[] args) {
        int[] arr = {4, 23, 54};
        int size = arr.length;
        System.out.println(turnSequence(arr, size));
    }

    public static String turnSequence(int[] arr, int size) {
        String result = "";
        if (size > 0) {
            result = String.valueOf(arr[size - 1]) + turnSequence(arr, --size);
        }
        return result;
    }
}
