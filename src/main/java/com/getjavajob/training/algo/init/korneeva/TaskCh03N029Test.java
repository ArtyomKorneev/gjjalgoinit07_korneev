package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh03N029.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 23.08.16.
 */
public class TaskCh03N029Test {
    public static void main(String[] args) {
        test1();
        test2();
        test3();
        test4();
        test5();
        test6();
    }

    private static void test1() {
        assertEquals("TaskCh03N029Test.test1", true, checkOdd(5, 7));
    }

    private static void test2() {
        assertEquals("TaskCh03N029Test.test2", true, lessThanTwenty(45, 18));
    }

    private static void test3() {
        assertEquals("TaskCh03N029Test.test3", true, checkZero(0, 15));
    }

    private static void test4() {
        assertEquals("TaskCh03N029Test.test4", true, checkNegativity(-7, -4, -8));
    }

    private static void test5() {
        assertEquals("TaskCh03N029Test.test5", true, divideIntoFive(45, 18, 24));
    }

    private static void test6() {
        assertEquals("TaskCh03N029Test.test6", true, moreThanAHundred(0, 15, 124));
    }
}
