package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 04.08.16.
 */
public class TaskCh10N050 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the n:");
        int n = scanner.nextInt();
        System.out.println("Enter the m:");
        int m = scanner.nextInt();
        System.out.println(calculateAcker(n, m));
    }

    public static int calculateAcker(int n, int m) {
        if (n == 0) {
            return m + 1;
        } else if (n > 0 && m == 0) {
            return calculateAcker(n - 1, 1);
        } else {
            return calculateAcker(n - 1, calculateAcker(n, m - 1));
        }
    }
}
