package com.getjavajob.training.algo.init.korneeva;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by ZinZaga on 27.07.16.
 */
public class TaskCh09N166 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the sentence:");
        String sentence = scanner.nextLine();
        System.out.println(changeWords(sentence));
    }

    public static String changeWords(String sentence) {
        String[] lines = sentence.split("\\s");
        int total = lines.length - 1;
        String temp = lines[0];
        lines[0] = lines[total];
        lines[total] = temp;
        return Arrays.toString(lines);
    }
}
