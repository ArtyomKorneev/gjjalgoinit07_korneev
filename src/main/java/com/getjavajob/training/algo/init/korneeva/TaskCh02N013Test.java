package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh02N013.fromRightToLeft;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

public class TaskCh02N013Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        int result = fromRightToLeft(123);
        assertEquals("TaskCh02N013Test", 321, result);
    }
}
