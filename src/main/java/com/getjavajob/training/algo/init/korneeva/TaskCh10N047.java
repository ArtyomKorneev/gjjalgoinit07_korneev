package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 04.08.16.
 */
public class TaskCh10N047 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the k:");
        int k = scanner.nextInt();
        System.out.println(fibonacciCalculation(k));
    }

    public static int fibonacciCalculation(int k) {
        if (k == 0) {
            return 1;
        } else if (k == 1) {
            return 1;
        } else {
            return fibonacciCalculation(k - 2) + fibonacciCalculation(k - 1);
        }
    }
}
