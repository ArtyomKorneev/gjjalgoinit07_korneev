package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh11N158.removeDuplicates;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 12.08.16.
 */
public class TaskCh11N158Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh11N158Test.test", new int[]{4, 45, 65, 12, 7, 0, 0, 0}, removeDuplicates(new int[]{4, 4, 45, 65, 45, 12, 7, 7}));
    }
}
