package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 19.07.16.
 */
public class TaskCh02N043 {
    public static void main(String[] args) {
        System.out.println("Enter two numbers");
        Scanner scanner = new Scanner(System.in);
        int numberOne = scanner.nextInt();
        int numberTwo = scanner.nextInt();
        int result = numberIsDevidedByTheNumber(numberOne, numberTwo);
        System.out.println(result);
    }

    public static int numberIsDevidedByTheNumber(int numberOne, int numberTwo) {
        return (numberOne % numberTwo) * (numberTwo % numberOne) + 1;
    }
}
