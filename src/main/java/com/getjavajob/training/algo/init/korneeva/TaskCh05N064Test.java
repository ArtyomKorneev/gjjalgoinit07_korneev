package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh05N064.getPopulationDensity;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 09.08.16.
 */
public class TaskCh05N064Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        int[] citizens = {4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};
        int[] areas = {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2};
        assertEquals("TaskCh05N064Test.test", 2, getPopulationDensity(citizens, areas));
    }
}
