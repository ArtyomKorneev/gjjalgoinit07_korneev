package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 11.08.16.
 */
public class TaskCh10N052 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number:");
        int number = scanner.nextInt();
        System.out.println(numberInReverseOrder(number));
    }

    public static String numberInReverseOrder(int number) {
        if (number == 0) {
            return "";
        } else {
            return String.valueOf(number % 10) + numberInReverseOrder(number / 10);
        }
    }
}
