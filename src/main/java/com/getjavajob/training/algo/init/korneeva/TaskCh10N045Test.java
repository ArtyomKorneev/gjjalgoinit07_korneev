package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh10N045.arithmeticProgression;
import static com.getjavajob.training.algo.init.korneeva.TaskCh10N045.sumOfThree;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 10.08.16.
 */
public class TaskCh10N045Test {
    public static void main(String[] args) {
        test1();
        test2();
    }

    private static void test1() {
        assertEquals("TaskCh10N045Test.test", 9, arithmeticProgression(1, 2, 5));
    }

    private static void test2() {
        assertEquals("TaskCh10N045Test.test", 25, sumOfThree(1, 2, 5));
    }
}
