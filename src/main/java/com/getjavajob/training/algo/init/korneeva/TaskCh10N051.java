package com.getjavajob.training.algo.init.korneeva;

/**
 * Created by ZinZaga on 04.08.16.
 */
public class TaskCh10N051 {
    public static void main(String[] args) {
        rec(5);
    }

    private static void rec(int n) {
        if (n > 0) {
            System.out.println(n);
            rec(n - 1);
            System.out.println(n);
        }
    }
}