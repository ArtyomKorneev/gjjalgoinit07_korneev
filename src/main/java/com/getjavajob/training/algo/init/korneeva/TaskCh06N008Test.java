package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh06N008.getUpTo;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 09.08.16.
 */
public class TaskCh06N008Test {
    public static void main(String[] args) {
        test();
    }

    private static void test() {
        assertEquals("TaskCh06N008Test.test", new double[]{1.0, 4.0, 9.0, 16.0, 25.0}, getUpTo(30));
    }
}
