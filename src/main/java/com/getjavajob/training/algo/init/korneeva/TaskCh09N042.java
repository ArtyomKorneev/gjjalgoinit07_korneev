package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 27.07.16.
 */
public class TaskCh09N042 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the word:");
        String word = scanner.nextLine();
        System.out.println(turn(word));
    }

    public static String turn(String word) {
        StringBuilder back = new StringBuilder(word);
        back.reverse();
        return String.valueOf(back);
    }
}
