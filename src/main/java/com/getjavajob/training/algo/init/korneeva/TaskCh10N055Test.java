package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh10N055.decimalToBinary;
import static com.getjavajob.training.algo.init.korneeva.TaskCh10N055.decimalToHexadecimal;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 24.08.16.
 */
public class TaskCh10N055Test {
    public static void main(String[] args) {
        test1();
        test2();
    }

    private static void test1() {
        assertEquals("TaskCh10N055Test.test1", "11000", decimalToBinary(24));
    }

    private static void test2() {
        assertEquals("TaskCh10N055Test.test2", "7FFF", decimalToHexadecimal(32767));
    }
}
