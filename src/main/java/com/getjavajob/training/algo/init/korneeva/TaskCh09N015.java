package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 27.07.16.
 */
public class TaskCh09N015 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the word:");
        String word = scanner.nextLine();
        System.out.println("Which do you prefer a symbol?");
        int symbol = scanner.nextInt();

        System.out.println(getSymbol(word, symbol));
    }

    public static char getSymbol(String word, int symbol) {
        return word.charAt(symbol - 1);
    }
}
