package com.getjavajob.training.algo.init.korneeva;

import static com.getjavajob.training.algo.init.korneeva.TaskCh04N067.weekendsOrWorkdays;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 09.08.16.
 */
public class TaskCh04N067Test {
    public static void main(String[] args) {
        test1();
        test2();
    }

    private static void test1() {
        assertEquals("TaskCh04N067Test.test1", "Workday", weekendsOrWorkdays(5));
    }

    private static void test2() {
        assertEquals("TaskCh04N067Test.test1", "Weekend", weekendsOrWorkdays(7));
    }
}
