package com.getjavajob.training.algo.init.korneeva;

import java.util.Scanner;

/**
 * Created by ZinZaga on 19.07.16.
 */
public class TaskCh04N015 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("What is month now?");
        int monthNow = scanner.nextInt();
        System.out.println("What is year now?");
        int yearNow = scanner.nextInt();
        System.out.println("What is month of your birthday?");
        int monthOfBirthday = scanner.nextInt();
        System.out.println("What is year of your birthday?");
        int yearOfBirthday = scanner.nextInt();
        int result = calculateAge(monthNow, yearNow, monthOfBirthday, yearOfBirthday);
        System.out.println(result);
    }

    public static int calculateAge(int monthNow, int yearNow, int monthOfBirthday, int yearOfBirthday) {
        int result = yearNow - yearOfBirthday;
        if (monthNow >= monthOfBirthday) {
            return result;
        } else {
            return result - 1;
        }
    }
}

